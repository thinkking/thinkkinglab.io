+++
categories = ["art"]
date = 2016-02-14T10:41:58Z
draft = true
title = "Good to Great Book Review"

+++
I read **Good to Great in January 2016**. An awesome read sharing detailed analysis on how good companies became great.